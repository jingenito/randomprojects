﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BaseSelect = New System.Windows.Forms.NumericUpDown()
        Me.lblChunk = New System.Windows.Forms.Label()
        Me.rbNone = New System.Windows.Forms.RadioButton()
        Me.rb4 = New System.Windows.Forms.RadioButton()
        Me.rb8 = New System.Windows.Forms.RadioButton()
        Me.rb16 = New System.Windows.Forms.RadioButton()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.txtNum = New System.Windows.Forms.TextBox()
        Me.txtOut = New System.Windows.Forms.TextBox()
        Me.lblBase = New System.Windows.Forms.Label()
        Me.lblNum = New System.Windows.Forms.Label()
        Me.lblOut = New System.Windows.Forms.Label()
        CType(Me.BaseSelect, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BaseSelect
        '
        Me.BaseSelect.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BaseSelect.Location = New System.Drawing.Point(323, 60)
        Me.BaseSelect.Maximum = New Decimal(New Integer() {16, 0, 0, 0})
        Me.BaseSelect.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.BaseSelect.Name = "BaseSelect"
        Me.BaseSelect.Size = New System.Drawing.Size(72, 38)
        Me.BaseSelect.TabIndex = 0
        Me.BaseSelect.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'lblChunk
        '
        Me.lblChunk.AutoSize = True
        Me.lblChunk.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChunk.Location = New System.Drawing.Point(601, 60)
        Me.lblChunk.Name = "lblChunk"
        Me.lblChunk.Size = New System.Drawing.Size(161, 31)
        Me.lblChunk.TabIndex = 1
        Me.lblChunk.Text = "Chunk Size:"
        '
        'rbNone
        '
        Me.rbNone.AutoSize = True
        Me.rbNone.Location = New System.Drawing.Point(793, 28)
        Me.rbNone.Name = "rbNone"
        Me.rbNone.Size = New System.Drawing.Size(51, 17)
        Me.rbNone.TabIndex = 2
        Me.rbNone.TabStop = True
        Me.rbNone.Text = "None"
        Me.rbNone.UseVisualStyleBackColor = True
        '
        'rb4
        '
        Me.rb4.AutoSize = True
        Me.rb4.Location = New System.Drawing.Point(793, 51)
        Me.rb4.Name = "rb4"
        Me.rb4.Size = New System.Drawing.Size(31, 17)
        Me.rb4.TabIndex = 3
        Me.rb4.TabStop = True
        Me.rb4.Text = "4"
        Me.rb4.UseVisualStyleBackColor = True
        '
        'rb8
        '
        Me.rb8.AutoSize = True
        Me.rb8.Location = New System.Drawing.Point(793, 74)
        Me.rb8.Name = "rb8"
        Me.rb8.Size = New System.Drawing.Size(31, 17)
        Me.rb8.TabIndex = 4
        Me.rb8.TabStop = True
        Me.rb8.Text = "8"
        Me.rb8.UseVisualStyleBackColor = True
        '
        'rb16
        '
        Me.rb16.AutoSize = True
        Me.rb16.Location = New System.Drawing.Point(793, 97)
        Me.rb16.Name = "rb16"
        Me.rb16.Size = New System.Drawing.Size(37, 17)
        Me.rb16.TabIndex = 5
        Me.rb16.TabStop = True
        Me.rb16.Text = "16"
        Me.rb16.UseVisualStyleBackColor = True
        '
        'btnStart
        '
        Me.btnStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStart.Location = New System.Drawing.Point(323, 377)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(301, 47)
        Me.btnStart.TabIndex = 6
        Me.btnStart.Text = "Calculate"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'txtNum
        '
        Me.txtNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNum.Location = New System.Drawing.Point(323, 201)
        Me.txtNum.Name = "txtNum"
        Me.txtNum.Size = New System.Drawing.Size(501, 38)
        Me.txtNum.TabIndex = 9
        '
        'txtOut
        '
        Me.txtOut.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOut.Location = New System.Drawing.Point(323, 276)
        Me.txtOut.Name = "txtOut"
        Me.txtOut.ReadOnly = True
        Me.txtOut.Size = New System.Drawing.Size(501, 38)
        Me.txtOut.TabIndex = 10
        '
        'lblBase
        '
        Me.lblBase.AutoSize = True
        Me.lblBase.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBase.Location = New System.Drawing.Point(157, 60)
        Me.lblBase.Name = "lblBase"
        Me.lblBase.Size = New System.Drawing.Size(84, 31)
        Me.lblBase.TabIndex = 11
        Me.lblBase.Text = "Base:"
        '
        'lblNum
        '
        Me.lblNum.AutoSize = True
        Me.lblNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNum.Location = New System.Drawing.Point(123, 204)
        Me.lblNum.Name = "lblNum"
        Me.lblNum.Size = New System.Drawing.Size(118, 31)
        Me.lblNum.TabIndex = 12
        Me.lblNum.Text = "Number:"
        '
        'lblOut
        '
        Me.lblOut.AutoSize = True
        Me.lblOut.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOut.Location = New System.Drawing.Point(81, 279)
        Me.lblOut.Name = "lblOut"
        Me.lblOut.Size = New System.Drawing.Size(160, 31)
        Me.lblOut.TabIndex = 13
        Me.lblOut.Text = "Conversion:"
        '
        'frmUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(869, 471)
        Me.Controls.Add(Me.lblOut)
        Me.Controls.Add(Me.lblNum)
        Me.Controls.Add(Me.lblBase)
        Me.Controls.Add(Me.txtOut)
        Me.Controls.Add(Me.txtNum)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.rb16)
        Me.Controls.Add(Me.rb8)
        Me.Controls.Add(Me.rb4)
        Me.Controls.Add(Me.rbNone)
        Me.Controls.Add(Me.lblChunk)
        Me.Controls.Add(Me.BaseSelect)
        Me.Name = "frmUI"
        Me.Text = "UI"
        CType(Me.BaseSelect, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BaseSelect As NumericUpDown
    Friend WithEvents lblChunk As Label
    Friend WithEvents rbNone As RadioButton
    Friend WithEvents rb4 As RadioButton
    Friend WithEvents rb8 As RadioButton
    Friend WithEvents rb16 As RadioButton
    Friend WithEvents btnStart As Button
    Friend WithEvents txtNum As TextBox
    Friend WithEvents txtOut As TextBox
    Friend WithEvents lblBase As Label
    Friend WithEvents lblNum As Label
    Friend WithEvents lblOut As Label
End Class
