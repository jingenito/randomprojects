﻿Public Class frmUI
    Private _max
    Property max As Integer
        Get
            Return _max
        End Get
        Set(value As Integer)
            _max = value
        End Set
    End Property

    Property chunk As Integer
        Get
            If rb4.Checked And showHidden Then
                Return 4
            ElseIf rb8.Checked And showHidden Then
                Return 8
            ElseIf rb16.Checked And showHidden Then
                Return 16
            Else
                Return -1
            End If
        End Get
        Set(value As Integer)

        End Set
    End Property

    Property showHidden As Boolean
        Get
            Select Case BaseSelect.Value
                Case 2
                    Return True
                Case 8
                    Return True
                Case 16
                    Return True
                Case Else
                    Return False
            End Select
        End Get
        Set(value As Boolean)

        End Set
    End Property

    Dim hiddencontent As New List(Of Object)
    Private Sub frmUI_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        max = 32
        hiddencontent.Add(lblChunk)
        hiddencontent.Add(rbNone)
        hiddencontent.Add(rb4)
        hiddencontent.Add(rb8)
        hiddencontent.Add(rb16)
        rbNone.Checked = True
    End Sub

    Private Sub BaseSelect_ValueChanged(sender As Object, e As EventArgs) Handles BaseSelect.ValueChanged
        ChangeVisibility()
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        If txtNum.Text >= 0 And txtNum.Text < 2147483648 Then
            txtOut.Text = convertBase(txtNum.Text, BaseSelect.Value)
        Else
            txtOut.Text = "Overflow"
        End If
    End Sub

    Private Sub ChangeVisibility()
        For Each h In hiddencontent
            h.visible = showHidden
        Next
    End Sub

    Private Function getDigits(num As Integer, base As Integer) As Integer
        If showHidden Then
            For i = 1 To max
                If (base ^ i) > num And (i Mod chunk) = 0 Then
                    Return i - 1
                End If
            Next
        Else
            For i = 1 To max
                If (base ^ i) > num Then
                    Return i - 1
                End If
            Next
        End If
        Return -1
    End Function

    Private Function convertBase(num As Integer, base As Integer) As String
        Dim s = ""
        For i = getDigits(num, base) To 0 Step -1
            Dim val = base ^ i
            If val <= num Then
                Dim dig = (num / val) - (num Mod val) / val
                If applyFormat(i) Then
                    s &= applyHex(dig) & " "
                Else
                    s &= applyHex(dig)
                End If
                num -= (dig * val)
            Else
                If applyFormat(i) Then
                    s &= "0 "
                Else
                    s &= "0"
                End If
            End If
        Next
        Return s
    End Function

    Private Function applyHex(x As Integer) As String
        Select Case x
            Case 10
                Return "a"
            Case 11
                Return "b"
            Case 12
                Return "c"
            Case 13
                Return "d"
            Case 14
                Return "e"
            Case 15
                Return "f"
            Case Else
                Return x.ToString
        End Select
    End Function

    Private Function applyFormat(i As Integer) As Boolean
        If chunk > 0 Then
            Return If(i Mod chunk = 0, True, False)
        End If
        Return False
    End Function
End Class
