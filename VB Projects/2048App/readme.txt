This is a replication of the 2048 app I'm coing for fun
It's coded kind of sloppily at first because I made it really fast
Now I'm working on making it more sophisticated that the heart of it is complete

Method:
-Uses a one-dimensional array that breaks a 4x4 board into a linear fashion (easier mathematically than to have a 4x4 array)
-Indices 0 through 15
-Move conversions:
	Up - subt 4
	Down - add 4
	Left - subt 1
	Right - add 1
-Right/left edge checking:
	Right edge - 3, 7, 11, 15
		or when (n + 1) mod 4 = 0
	Left edge - 0, 4, 8, 12
		or when n mod 4 = 0

Future Thoughts:
-Create a control called TileGrid
	-include TilesChanged event
	-include a Win event
	-include a Loose event
	-source is the values array on frmUI
	-move all methods into the control
	-frmUI updates based off of raised events