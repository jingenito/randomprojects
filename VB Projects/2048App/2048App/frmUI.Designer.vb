﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblMsg = New System.Windows.Forms.Label()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.btnUp = New System.Windows.Forms.Button()
        Me.btnDown = New System.Windows.Forms.Button()
        Me.btnRight = New System.Windows.Forms.Button()
        Me.btnLeft = New System.Windows.Forms.Button()
        Me.pbxLogo = New System.Windows.Forms.PictureBox()
        Me.tile12 = New System.Windows.Forms.PictureBox()
        Me.tile2 = New System.Windows.Forms.PictureBox()
        Me.tile11 = New System.Windows.Forms.PictureBox()
        Me.tile0 = New System.Windows.Forms.PictureBox()
        Me.tile15 = New System.Windows.Forms.PictureBox()
        Me.tile5 = New System.Windows.Forms.PictureBox()
        Me.tile14 = New System.Windows.Forms.PictureBox()
        Me.tile6 = New System.Windows.Forms.PictureBox()
        Me.tile13 = New System.Windows.Forms.PictureBox()
        Me.tile8 = New System.Windows.Forms.PictureBox()
        Me.tile1 = New System.Windows.Forms.PictureBox()
        Me.tile9 = New System.Windows.Forms.PictureBox()
        Me.tile10 = New System.Windows.Forms.PictureBox()
        Me.tile3 = New System.Windows.Forms.PictureBox()
        Me.tile7 = New System.Windows.Forms.PictureBox()
        Me.tile4 = New System.Windows.Forms.PictureBox()
        Me.lblScore = New System.Windows.Forms.Label()
        Me.scoretxt = New System.Windows.Forms.Label()
        CType(Me.pbxLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tile4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblMsg
        '
        Me.lblMsg.AutoSize = True
        Me.lblMsg.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg.Location = New System.Drawing.Point(8, 101)
        Me.lblMsg.Name = "lblMsg"
        Me.lblMsg.Size = New System.Drawing.Size(74, 20)
        Me.lblMsg.TabIndex = 17
        Me.lblMsg.Text = "Message"
        '
        'btnStart
        '
        Me.btnStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStart.Location = New System.Drawing.Point(333, 92)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(104, 39)
        Me.btnStart.TabIndex = 18
        Me.btnStart.Text = "New Game"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'btnUp
        '
        Me.btnUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUp.Location = New System.Drawing.Point(191, 502)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(76, 61)
        Me.btnUp.TabIndex = 20
        Me.btnUp.Text = "Up"
        Me.btnUp.UseVisualStyleBackColor = True
        '
        'btnDown
        '
        Me.btnDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDown.Location = New System.Drawing.Point(191, 569)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(76, 61)
        Me.btnDown.TabIndex = 21
        Me.btnDown.Text = "Down"
        Me.btnDown.UseVisualStyleBackColor = True
        '
        'btnRight
        '
        Me.btnRight.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRight.Location = New System.Drawing.Point(274, 569)
        Me.btnRight.Name = "btnRight"
        Me.btnRight.Size = New System.Drawing.Size(76, 61)
        Me.btnRight.TabIndex = 22
        Me.btnRight.Text = "Right"
        Me.btnRight.UseVisualStyleBackColor = True
        '
        'btnLeft
        '
        Me.btnLeft.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLeft.Location = New System.Drawing.Point(109, 569)
        Me.btnLeft.Name = "btnLeft"
        Me.btnLeft.Size = New System.Drawing.Size(76, 61)
        Me.btnLeft.TabIndex = 23
        Me.btnLeft.Text = "Left"
        Me.btnLeft.UseVisualStyleBackColor = True
        '
        'pbxLogo
        '
        Me.pbxLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbxLogo.Image = Global._2048App.My.Resources.Resources.Logo
        Me.pbxLogo.Location = New System.Drawing.Point(12, 12)
        Me.pbxLogo.Name = "pbxLogo"
        Me.pbxLogo.Size = New System.Drawing.Size(145, 62)
        Me.pbxLogo.TabIndex = 0
        Me.pbxLogo.TabStop = False
        '
        'tile12
        '
        Me.tile12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile12.Location = New System.Drawing.Point(27, 413)
        Me.tile12.Name = "tile12"
        Me.tile12.Size = New System.Drawing.Size(96, 83)
        Me.tile12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile12.TabIndex = 56
        Me.tile12.TabStop = False
        '
        'tile2
        '
        Me.tile2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile2.Location = New System.Drawing.Point(231, 146)
        Me.tile2.Name = "tile2"
        Me.tile2.Size = New System.Drawing.Size(96, 83)
        Me.tile2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile2.TabIndex = 50
        Me.tile2.TabStop = False
        '
        'tile11
        '
        Me.tile11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile11.Location = New System.Drawing.Point(333, 324)
        Me.tile11.Name = "tile11"
        Me.tile11.Size = New System.Drawing.Size(96, 83)
        Me.tile11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile11.TabIndex = 55
        Me.tile11.TabStop = False
        '
        'tile0
        '
        Me.tile0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile0.Location = New System.Drawing.Point(27, 146)
        Me.tile0.Name = "tile0"
        Me.tile0.Size = New System.Drawing.Size(96, 83)
        Me.tile0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile0.TabIndex = 41
        Me.tile0.TabStop = False
        '
        'tile15
        '
        Me.tile15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile15.Location = New System.Drawing.Point(333, 413)
        Me.tile15.Name = "tile15"
        Me.tile15.Size = New System.Drawing.Size(96, 83)
        Me.tile15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile15.TabIndex = 54
        Me.tile15.TabStop = False
        '
        'tile5
        '
        Me.tile5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile5.Location = New System.Drawing.Point(129, 235)
        Me.tile5.Name = "tile5"
        Me.tile5.Size = New System.Drawing.Size(96, 83)
        Me.tile5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile5.TabIndex = 42
        Me.tile5.TabStop = False
        '
        'tile14
        '
        Me.tile14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile14.Location = New System.Drawing.Point(231, 413)
        Me.tile14.Name = "tile14"
        Me.tile14.Size = New System.Drawing.Size(96, 83)
        Me.tile14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile14.TabIndex = 53
        Me.tile14.TabStop = False
        '
        'tile6
        '
        Me.tile6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile6.Location = New System.Drawing.Point(231, 235)
        Me.tile6.Name = "tile6"
        Me.tile6.Size = New System.Drawing.Size(96, 83)
        Me.tile6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile6.TabIndex = 43
        Me.tile6.TabStop = False
        '
        'tile13
        '
        Me.tile13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile13.Location = New System.Drawing.Point(129, 413)
        Me.tile13.Name = "tile13"
        Me.tile13.Size = New System.Drawing.Size(96, 83)
        Me.tile13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile13.TabIndex = 52
        Me.tile13.TabStop = False
        '
        'tile8
        '
        Me.tile8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile8.Location = New System.Drawing.Point(27, 324)
        Me.tile8.Name = "tile8"
        Me.tile8.Size = New System.Drawing.Size(96, 83)
        Me.tile8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile8.TabIndex = 44
        Me.tile8.TabStop = False
        '
        'tile1
        '
        Me.tile1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile1.Location = New System.Drawing.Point(129, 146)
        Me.tile1.Name = "tile1"
        Me.tile1.Size = New System.Drawing.Size(96, 83)
        Me.tile1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile1.TabIndex = 51
        Me.tile1.TabStop = False
        '
        'tile9
        '
        Me.tile9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile9.Location = New System.Drawing.Point(129, 324)
        Me.tile9.Name = "tile9"
        Me.tile9.Size = New System.Drawing.Size(96, 83)
        Me.tile9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile9.TabIndex = 45
        Me.tile9.TabStop = False
        '
        'tile10
        '
        Me.tile10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile10.Location = New System.Drawing.Point(231, 324)
        Me.tile10.Name = "tile10"
        Me.tile10.Size = New System.Drawing.Size(96, 83)
        Me.tile10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile10.TabIndex = 46
        Me.tile10.TabStop = False
        '
        'tile3
        '
        Me.tile3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile3.Location = New System.Drawing.Point(333, 146)
        Me.tile3.Name = "tile3"
        Me.tile3.Size = New System.Drawing.Size(96, 83)
        Me.tile3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile3.TabIndex = 49
        Me.tile3.TabStop = False
        '
        'tile7
        '
        Me.tile7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile7.Location = New System.Drawing.Point(333, 235)
        Me.tile7.Name = "tile7"
        Me.tile7.Size = New System.Drawing.Size(96, 83)
        Me.tile7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile7.TabIndex = 47
        Me.tile7.TabStop = False
        '
        'tile4
        '
        Me.tile4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tile4.Location = New System.Drawing.Point(27, 235)
        Me.tile4.Name = "tile4"
        Me.tile4.Size = New System.Drawing.Size(96, 83)
        Me.tile4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tile4.TabIndex = 48
        Me.tile4.TabStop = False
        '
        'lblScore
        '
        Me.lblScore.AutoSize = True
        Me.lblScore.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore.Location = New System.Drawing.Point(284, 30)
        Me.lblScore.Name = "lblScore"
        Me.lblScore.Size = New System.Drawing.Size(55, 20)
        Me.lblScore.TabIndex = 57
        Me.lblScore.Text = "Score:"
        '
        'scoretxt
        '
        Me.scoretxt.AutoSize = True
        Me.scoretxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.scoretxt.Location = New System.Drawing.Point(357, 30)
        Me.scoretxt.Name = "scoretxt"
        Me.scoretxt.Size = New System.Drawing.Size(48, 20)
        Me.scoretxt.TabIndex = 58
        Me.scoretxt.Text = "score"
        '
        'frmUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(458, 639)
        Me.Controls.Add(Me.scoretxt)
        Me.Controls.Add(Me.lblScore)
        Me.Controls.Add(Me.tile12)
        Me.Controls.Add(Me.tile2)
        Me.Controls.Add(Me.tile11)
        Me.Controls.Add(Me.tile0)
        Me.Controls.Add(Me.tile15)
        Me.Controls.Add(Me.tile5)
        Me.Controls.Add(Me.tile14)
        Me.Controls.Add(Me.tile6)
        Me.Controls.Add(Me.tile13)
        Me.Controls.Add(Me.tile8)
        Me.Controls.Add(Me.tile1)
        Me.Controls.Add(Me.tile9)
        Me.Controls.Add(Me.tile10)
        Me.Controls.Add(Me.tile3)
        Me.Controls.Add(Me.tile7)
        Me.Controls.Add(Me.tile4)
        Me.Controls.Add(Me.btnLeft)
        Me.Controls.Add(Me.btnRight)
        Me.Controls.Add(Me.btnDown)
        Me.Controls.Add(Me.btnUp)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.lblMsg)
        Me.Controls.Add(Me.pbxLogo)
        Me.KeyPreview = True
        Me.Name = "frmUI"
        Me.Text = "2048"
        CType(Me.pbxLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tile4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pbxLogo As PictureBox
    Friend WithEvents lblMsg As Label
    Friend WithEvents btnStart As Button
    Friend WithEvents btnUp As Button
    Friend WithEvents btnDown As Button
    Friend WithEvents btnRight As Button
    Friend WithEvents btnLeft As Button
    Friend WithEvents tile12 As PictureBox
    Friend WithEvents tile2 As PictureBox
    Friend WithEvents tile11 As PictureBox
    Friend WithEvents tile0 As PictureBox
    Friend WithEvents tile15 As PictureBox
    Friend WithEvents tile5 As PictureBox
    Friend WithEvents tile14 As PictureBox
    Friend WithEvents tile6 As PictureBox
    Friend WithEvents tile13 As PictureBox
    Friend WithEvents tile8 As PictureBox
    Friend WithEvents tile1 As PictureBox
    Friend WithEvents tile9 As PictureBox
    Friend WithEvents tile10 As PictureBox
    Friend WithEvents tile3 As PictureBox
    Friend WithEvents tile7 As PictureBox
    Friend WithEvents tile4 As PictureBox
    Friend WithEvents lblScore As Label
    Friend WithEvents scoretxt As Label
End Class
