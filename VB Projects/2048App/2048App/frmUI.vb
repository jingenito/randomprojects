﻿Public Class frmUI

    WithEvents TG As New TileGrid

    Public Sub InitLists()
        'creates list of tiles (pictureboxes)
        'need if statement because the sub is not always called from a load event
        Dim tilelist = {tile0, tile1, tile2, tile3, tile4, tile5, tile6, tile7, tile8, tile9, tile10, tile11, tile12, tile13, tile14, tile15}
        Dim values(15) As Integer
        For i = 0 To 15
            values(i) = 0
        Next
        TG.gridtiles = New Tiles With {.tiles = tilelist.ToList, .values = values, .score = 0}
    End Sub

    Private Sub frmUI_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TG.msg = "Join the numbers and get to the 2048 Tile!"
        TG.score = "0"
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        'calls generate function twice to generate two tiles at start
        InitLists()
        TG.gridtiles.GenerateTile()
        TG.gridtiles.GenerateTile()
        TG.score = TG.gridtiles.score.ToString
        TG.gridtiles.DisplayImages()
    End Sub

    Private Sub TG_OnWin(sender As Object, e As TileEventArgs) Handles TG.OnWin
        'Temporary Win Event - subject to change
        If e.highest < 8192 Then
            MsgBox("You have reached the " & e.highest.ToString & " tile!")
        Else
            MsgBox("You have reached the highest tile!")
            btnStart.PerformClick()
        End If
    End Sub

    Private Sub TG_OnLoose(sender As Object, e As EventArgs) Handles TG.OnLoose
        'Temporary Loose Event - subject to change
        'temp msg teehee
        MsgBox("Sorry loser")
        btnStart.PerformClick()
    End Sub

    Private Sub btnUp_Click(sender As Object, e As EventArgs) Handles btnUp.Click
        Dim move = False
        TG.gridtiles.Movement("up", move)
        TG.Update(move)
    End Sub

    Private Sub btnDown_Click(sender As Object, e As EventArgs) Handles btnDown.Click
        Dim move = False
        TG.gridtiles.Movement("down", move)
        TG.Update(move)
    End Sub

    Private Sub btnLeft_Click(sender As Object, e As EventArgs) Handles btnLeft.Click
        Dim move = False
        TG.gridtiles.Movement("left", move)
        TG.Update(move)
    End Sub

    Private Sub btnRight_Click(sender As Object, e As EventArgs) Handles btnRight.Click
        Dim move = False
        TG.gridtiles.Movement("right", move)
        TG.Update(move)
    End Sub

End Class

Public Class TileGrid

    Public Delegate Sub TileEvents(sender As Object, e As TileEventArgs)
    Public Event OnWin As TileEvents
    Public Event OnLoose As TileEvents

    Public Sub TileGrid()
        highest = 0
    End Sub

    WriteOnly Property msg As String
        Set(value As String)
            frmUI.lblMsg.Text = value
        End Set
    End Property

    ReadOnly Property goal
        Get
            'returns the goal value based on the highest current tile
            If highest >= 2048 Then
                Return highest * 2
            Else
                Return 2048
            End If
        End Get
    End Property

    Private _highest
    Property highest As Integer
        Get
            Return _highest
        End Get
        Set(value As Integer)
            _highest = value
        End Set
    End Property

    Private _gridtiles
    Property gridtiles As Tiles
        Get
            Return _gridtiles
        End Get
        Set(value As Tiles)
            _gridtiles = value
        End Set
    End Property

    Property score As String
        Get
            Return frmUI.scoretxt.Text
        End Get
        Set(value As String)
            frmUI.scoretxt.Text = value
        End Set
    End Property

    Public Sub Update(move As Boolean)
        If move Then
            gridtiles.GenerateTile()
            gridtiles.DisplayImages()
            UpdateHighest()
            score = gridtiles.score.ToString
            msg = "Join the numbers and get to the " & goal.ToString & " Tile!"
            CheckForWinLoose()
        End If
    End Sub

    Private Sub UpdateHighest()
        For Each v In gridtiles.values
            If v > highest Then highest = v
        Next
    End Sub

    Private Sub CheckForWinLoose()
        Dim e = New TileEventArgs With {.highest = highest}
        If (goal / 2) = highest Then RaiseEvent OnWin(Me, e)
        For i = 4 To 15
            If gridtiles.canMove("up", i) Or gridtiles.values(i) = 0 Then Exit Sub
        Next
        For i = 0 To 11
            If gridtiles.canMove("down", i) Or gridtiles.values(i) = 0 Then Exit Sub
        Next
        For i = 1 To 15
            If i Mod 4 <> 0 Then
                If gridtiles.canMove("left", i) Or gridtiles.values(i) = 0 Then Exit Sub
            End If
        Next
        For i = 0 To 14
            If (i + 1) Mod 4 <> 0 Then
                If gridtiles.canMove("right", i) Or gridtiles.values(i) = 0 Then Exit Sub
            End If
        Next
        'can only reach here if there was no possible move
        RaiseEvent OnLoose(Me, e)
    End Sub

End Class

Public Class TileEventArgs
    Inherits EventArgs
    Public highest As Integer
    Public score As Integer
End Class

Public Class Tiles
    Public tiles As List(Of PictureBox)
    Public values(15) As Integer
    Public score As Integer

#Region "Movement"
    'for all movement subs:
    'reason for value n is to continue moving each tile in the direction until it cannot move
    'value i is only to loop over each tile
    'move variable is so the event doesn't get raised before all the tiles finish moving
    '
    'appears buggy but movement issue has been checked and the lack of animations causes the movement
    'to appear as if tiles don't move all the way - but the illusion is from the new tile being generated
    'and the movement is snappy
    Public Sub Movement(ByVal s As String, ByRef move As Boolean)
        Select Case s
            Case "up"
                'start furthest up
                'i < 4 can't move up
                For i = 4 To 15
                    Dim n = i
                    'if statement is to skip over tiles that do not contain values
                    If values(n) <> 0 Then
                        While canMove(s, n)
                            score = If(values(n - 4) = values(n), score + values(n - 4) * 2, score)
                            values(n - 4) += values(n)
                            values(n) = 0
                            move = True
                            n -= 4
                            If n < 4 Then Exit While
                        End While
                    End If
                Next
            Case "down"
                'start furthest down
                'i > 11 can't move down
                For i = 11 To 0 Step -1
                    Dim n = i
                    If values(n) <> 0 Then
                        While canMove(s, n)
                            score = If(values(n + 4) = values(n), score + values(n + 4) * 2, score)
                            values(n + 4) += values(n)
                            values(n) = 0
                            move = True
                            n += 4
                            If n > 11 Then Exit While
                        End While
                    End If
                Next
            Case "left"
                'start furthest left
                'no need to check 0
                For i = 1 To 15
                    Dim n = i
                    If values(n) <> 0 Then
                        While canMove(s, n)
                            score = If(values(n - 1) = values(n), score + values(n - 1) * 2, score)
                            values(n - 1) += values(n)
                            values(n) = 0
                            move = True
                            n -= 1
                            If n = 0 Then Exit While
                        End While
                    End If
                Next
            Case "right"
                'start furthest right
                'no need to check 15
                For i = 14 To 0 Step -1
                    Dim n = i
                    If values(n) <> 0 Then
                        While canMove("right", n)
                            score = If(values(n + 1) = values(n), score + values(n + 1) * 2, score)
                            values(n + 1) += values(n)
                            values(n) = 0
                            move = True
                            n += 1
                            If n = 15 Then Exit While
                        End While
                    End If
                Next
        End Select
    End Sub

    Public Function canMove(s As String, n As Integer) As Boolean
        Select Case s
            Case "up"
                If (values(n - 4) = 0 Or values(n - 4) = values(n)) And n > 3 Then Return True
            Case "down"
                If (values(n + 4) = 0 Or values(n + 4) = values(n)) And n < 12 Then Return True
            Case "left"
                If (values(n - 1) = 0 Or values(n - 1) = values(n)) And n Mod 4 <> 0 Then Return True
                    'n mod 4 checks for left edge
            Case "right"
                If (values(n + 1) = 0 Or values(n + 1) = values(n)) And (n + 1) Mod 4 <> 0 Then Return True
                '(n + 1) mod 4 checks for right edge
        End Select
        Return False
    End Function
#End Region

#Region "Tile Generator"
    Private Function generate(s As String) As Integer
        'declared as static to ensure getting the next generated number
        Static r As New Random()
        Select Case s
            Case "tile"
                Dim temp = r.Next(1, 16) - 1
                While temp < 0 Or temp > 15
                    'keeps tile index in range
                    'this loop should never be entered - extra protection
                    temp = r.Next(1, 16) - 1
                End While
                Return temp
            Case "num"
                Select Case r.Next(2)
                    Case 0
                        Return 2
                    Case 1
                        Return 4
                    Case Else
                        'calls function recursively if not a desired result 
                        'done this way because this result should never even occur - extra protection
                        generate("num")
                End Select
        End Select
        'a failed result is -1
        Return -1
    End Function

    Public Sub GenerateTile()
        Dim t = generate("tile")
        While t < 0
            MsgBox("Clicked too fast")
            t = generate("tile")
        End While
        Dim n As New List(Of Integer)
        n.Add(t)
        'will call the worst case generate sub if only 1 tile remains
        While values(t) <> 0 And n.Count < 15
            'adds every checked index
            t = generate("tile")
            If Not n.Contains(t) Then n.Add(t)
            'continues until an empty tile is found
        End While
        If n.Count < 15 Then
            values(t) = generate("num")
            score += values(t)
        Else
            WorstCase()
        End If
    End Sub

    Private Sub WorstCase()
        For i = 0 To 15
            If values(i) = 0 Then
                values(i) = generate("num")
                score += values(i)
            End If
        Next
        'if no empty tiles then does not generate anything
        'was forced to do it like this to avoid an infinite while loop
    End Sub

#End Region

#Region "Imaging"
    Private Function getImage(x As Integer) As Object
        Select Case x
            Case 2
                Return My.Resources._2
            Case 4
                Return My.Resources._4
            Case 8
                Return My.Resources._8
            Case 16
                Return My.Resources._16
            Case 32
                Return My.Resources._32
            Case 64
                Return My.Resources._64
            Case 128
                Return My.Resources._128
            Case 256
                Return My.Resources._256
            Case 512
                Return My.Resources._512
            Case 1024
                Return My.Resources._1024
            Case 2048
                Return My.Resources._2048
            Case 4096
                Return My.Resources._4096
            Case 8192
                Return My.Resources._8192
            Case Else
                Return Nothing
        End Select
    End Function

    Public Sub DisplayImages()
        For i = 0 To 15
            tiles(i).Image = getImage(values(i))
        Next
    End Sub
#End Region

End Class